/*
 * Copyright Copyright 2019 Tom Warnke
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.toromtomtom.mitl

import com.toromtomtom.Trajectories
import org.scalatest.{FlatSpec, Matchers}

/**
  * @author Tom Warnke
  */
class CSV2MITL extends FlatSpec with Matchers {

  "Reading in a trajectory from a file and checking it" should "produce correct results" in {

    val file = this.getClass.getResource("/trajectory.csv").toURI
    val t = Trajectories.fromCSV(file)

    assertResult(true)(MITL.check(G(0.0, 2.0)(OutVar("a") > Constant(0.0)), t))
    assertResult(false)(MITL.check(G(0.0, 4.0)(OutVar("a") > Constant(0.0)), t))
    assertResult(true)(MITL.check(G(0.0, 4.0)(OutVar("a") >= Constant(0.0)), t))
    assertResult(false)(MITL.check(G(0.0, 7.0)(OutVar("a") >= Constant(0.0)), t))
    assertResult(true)(MITL.check(F(0.0, 7.0)(OutVar("a") > OutVar("b")), t))

  }

}
