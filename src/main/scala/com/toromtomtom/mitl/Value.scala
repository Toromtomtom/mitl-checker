/*
 * Copyright Copyright 2019 Tom Warnke
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.toromtomtom.mitl

/**
  * @author Tom Warnke
  */
sealed trait Value {
  def <(other: Value): MITLFormula = Comparison(this, other, _ < _)

  def <=(other: Value): MITLFormula = Comparison(this, other, _ <= _)

  def ===(other: Value): MITLFormula = Comparison(this, other, _ == _)

  def >=(other: Value): MITLFormula = Comparison(this, other, _ >= _)

  def >(other: Value): MITLFormula = Comparison(this, other, _ > _)
}

case class Constant(value: Double) extends Value

case class OutVar(observable: String) extends Value
