/*
 * Copyright Copyright 2019 Tom Warnke
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.toromtomtom.mitl

/**
  * @author Tom Warnke
  */
sealed trait MITLFormula {

  def and(other: MITLFormula): MITLFormula = Conjunction(this, other)

  def or(other: MITLFormula): MITLFormula = Disjunction(this, other)

  def unary_!(): MITLFormula = Negation(this)

  def U(from: Double, to: Double)(other: MITLFormula): MITLFormula = Until(from, to, this, other)

}

case class Comparison(left: Value,
                      right: Value,
                      op: (Double, Double) => Boolean) extends MITLFormula

case class Conjunction(left: MITLFormula,
                       right: MITLFormula) extends MITLFormula

case class Disjunction(left: MITLFormula,
                       right: MITLFormula) extends MITLFormula

case class Negation(formula: MITLFormula) extends MITLFormula

case class Globally(from: Double,
                    to: Double,
                    inner: MITLFormula) extends MITLFormula

case class Eventually(from: Double,
                      to: Double,
                      inner: MITLFormula) extends MITLFormula

case class Until(from: Double,
                 to: Double,
                 left: MITLFormula,
                 right: MITLFormula) extends MITLFormula

case class True() extends MITLFormula

// some helper objects
object G {
  def apply(from: Double, to: Double)
           (inner: MITLFormula): Globally = Globally(from, to, inner)
}

object F {
  def apply(from: Double, to: Double)
           (inner: MITLFormula): Eventually = Eventually(from, to, inner)
}