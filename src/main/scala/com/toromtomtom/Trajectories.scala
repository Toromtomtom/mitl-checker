package com.toromtomtom

import java.io.File
import java.net.URI
import java.nio.charset.Charset

import org.apache.commons.csv.{CSVFormat, CSVParser}

import scala.collection.JavaConverters._

/**
  * The functions in here assume that the CSV file to parse contains a header and then one line per time point.
  * One column called "t" represents the time point.
  * The other columns contain the observed variable values.
  *
  * @author Tom Warnke
  */
object Trajectories {

  val timePointColumnName = "t"

  def fromCSV(fileName: String): Map[String, List[(Double, Double)]] = fromCSV(new File(fileName))

  def fromCSV(file: File): Map[String, List[(Double, Double)]] = {
    val parser = CSVParser.parse(
      file,
      Charset.defaultCharset(),
      CSVFormat.DEFAULT.withHeader())
    val headerKeys = parser.getHeaderMap.keySet().asScala
    val variables = (headerKeys - timePointColumnName).toList

    val lines = for (record <- parser.iterator().asScala) yield {
      val time = record.get(timePointColumnName).toDouble
      for (v <- variables) yield (time, record.get(v).toDouble)
    }
    val columns = lines.toList.transpose
    variables.zip(columns).toMap
  }

  def fromCSV(uri: URI): Map[String, List[(Double, Double)]] = fromCSV(new File(uri))
}
