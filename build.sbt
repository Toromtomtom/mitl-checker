name := "MITL-Checker"

version := "0.1"

scalaVersion := "2.12.8"

libraryDependencies ++= Seq(
  "org.apache.commons" % "commons-csv" % "1.6",
  "org.scalatest" % "scalatest_2.12" % "3.0.1" % Test
)
  