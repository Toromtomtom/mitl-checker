# MITL-Checker

This is an implementation of an algorithm to check MITL formulas on trajectories in CSV files.
The algorithm has been described in:
Maler O., Nickovic D. (2004) [Monitoring Temporal Properties of Continuous Signals](https://doi.org/10.1007/978-3-540-30206-3_12).
In: Lakhnech Y., Yovine S. (eds) Formal Techniques, Modelling and Analysis of Timed and Fault-Tolerant Systems.
FTRTFT 2004, FORMATS 2004. Lecture Notes in Computer Science, vol 3253. Springer, Berlin, Heidelberg

The implementation is taken from the software project [SESSL](http://sessl.org) and has been documented in:
T. Warnke and A. M. Uhrmacher, ["Complex Simulation Experiments Made Easy"](https://doi.org/10.1109/WSC.2018.8632429) 2018 Winter Simulation Conference (WSC), Gothenburg, Sweden, 2018, pp. 410-424.

## Usage

The usage is demonstrated in the test MITL2CSV.scala under src/test/scala/com/toromtomtom/mitl. The important part is the following:

    val t = Trajectories.fromCSV(file) // where "file" is the CSV file path
    MITL.check(formula, t) // where formula is the MITL formula

